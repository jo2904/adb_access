# Adb Access
Ce programme n'est pas à utilisé pour des actions ilégales et le programmeur n'est pas respnsable de l'utilisation que vous en ferait.

Programme pour accéder aux périphériques SAMSUNG utilisant ADB 
Peut être utiliser en mod filaire ou via le reseau 

# Installation 

apt-get update
apt-get install python3 adb xterm
snap install scrcpy
python3 adb_access.py

# Activer adb sur votre téléphone

Pour utiliser ADB avec votre appareil Android, vous devez activer une fonction appelée débogage USB. 
Ouvrez le menu de votre téléphone, appuyez sur l'icône Paramètres et sélectionnez "À propos du téléphone". 
Faites défiler tout le chemin vers le bas et appuyez sept fois sur l'élément "Numéro de build". 
Vous devriez recevoir un message indiquant que vous êtes maintenant un développeur.
Retournez à la page Paramètres principaux, et vous devriez voir une nouvelle option près du bas appelée «Options de développeur». 
Ouvrez cela et activez "Débogage USB".


• Plus tard, lorsque vous connectez votre téléphone à votre ordinateur, vous verrez une fenêtre contextuelle intitulée «Autoriser le débogage USB?» 
Sur votre téléphone. Cochez la case «Toujours autoriser de cet ordinateur» et appuyez sur OK.

# Trouver des appareil sur shodan

Accéder à shodan.io
Se connecter
Rechercher "Android Device Bridge" (option: country:FR)

# Utilisation

Lancer le programme puis sélectionner la commande à effectée.
Entrer les paramètres.